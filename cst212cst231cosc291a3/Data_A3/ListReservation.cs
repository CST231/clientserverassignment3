﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data_A3
{
    public class ListReservation
    {
        public int artId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public decimal rentalPrice { get; set; }
        public string artist { get; set; }

        public ListReservation(int artId, string title, string description,
            decimal rentalPrice, string artist)
        {
            this.artId = artId;
            this.title = title;
            this.description = description;
            this.rentalPrice = rentalPrice;
            this.artist = artist;
        }
    }
}
