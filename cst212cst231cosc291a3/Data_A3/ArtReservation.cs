﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data_A3
{
    public class ArtReservation
    {
        public string title { get; set; }
        public decimal rentalPrice { get; set; }
        public int quarter { get; set; }

        public ArtReservation(string title, decimal rentalPrice, int quarter)
        {
            this.title = title;
            this.rentalPrice = rentalPrice;
            this.quarter = quarter;
        }
    }
}
