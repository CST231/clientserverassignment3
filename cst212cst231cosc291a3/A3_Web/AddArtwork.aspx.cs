﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Data_A3;
using System.Data.Linq;

namespace A3_Web
{
    public partial class AddArtwork : System.Web.UI.Page
    {
        CacheHelper ch;
        A3_LINQDataContext context = new A3_LINQDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            ch = new CacheHelper(this.Context);
            setUp();
        }

        private void setUp()
        {
            if (ch.sessionUser == null)
            {
                pnlAddArtwork.Visible = false;
                lblNotArtist.Visible = true;
            }
            else if (ch.sessionUser != null && ch.sessionUser.businessID > 1)
            {
                pnlAddArtwork.Visible = false;
                lblNotArtist.Visible = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Artwork newArt = new Artwork
            { 
                userID = ch.sessionUser.userID,
                title = txtTitle.Text,
                description = txtDescription.Text == "" ? null : txtDescription.Text,
                rentalPrice = Convert.ToDecimal(txtPrice.Text) 
            };
            context.Artworks.InsertOnSubmit(newArt);
            context.SubmitChanges();
            clearContents();
        }

        private void clearContents()
        {
            txtTitle.Text = "";
            txtDescription.Text = "";
            txtPrice.Text = "";
        }
    }
}