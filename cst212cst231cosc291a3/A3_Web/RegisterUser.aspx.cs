﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Data_A3;
using System.Data.Linq;
using wcm = System.Web.Configuration.WebConfigurationManager;
using System.Text.RegularExpressions;

namespace A3_Web
{
    public partial class RegisterUser : System.Web.UI.Page
    {
        A3_LINQDataContext context;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new A3_LINQDataContext();
        }

        private void clearContents()
        {
            ddlUserType.SelectedIndex = 0;
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtBusinessName.Text = "";
            txtEmailAddress.Text = "";
            txtPassword.Text = "";
            txtConfirm.Text = "";
        }

        protected void cusEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = Regex.IsMatch(args.Value, wcm.AppSettings["emailRegex"]);
            }
            catch
            {
                args.IsValid = false;
            }
        }

        protected void cusPassword_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = Regex.IsMatch(args.Value, wcm.AppSettings["passwordRegex"]);
            }
            catch
            {
                args.IsValid = false;
            }
        }

        protected void btnRegisterUser_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                Business business;
                User user = new User
                {
                    firstName = txtFirstName.Text,
                    lastName = txtLastName.Text,
                    email = txtEmailAddress.Text,
                    password = txtPassword.Text
                };
                if (ddlUserType.SelectedItem.Value.Equals("Artist"))
                {
                    business = context.Businesses
                        .First(b => b.businessID == 1);
                }
                else if (context.Businesses.Any(b => b.businessName == txtBusinessName.Text))
                {
                    business = context.Businesses
                        .First(b => b.businessName == txtBusinessName.Text);
                }
                else
                {
                    business = new Business
                    {
                        businessName = txtBusinessName.Text
                    };
                    context.Businesses.InsertOnSubmit(business);
                }
                business.Users.Add(user);
                context.SubmitChanges();
                clearContents();
            }
        }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlUserType.SelectedItem.Value.Equals("Artist"))
            {
                txtBusinessName.Enabled = false;
                txtBusinessName.Visible = false;
                lblBusinessName.Visible = false;
                rfvBusiness.Visible = false;
            }
            else
            {
                txtBusinessName.Enabled = true;
                txtBusinessName.Visible = true;
                lblBusinessName.Visible = true;
                rfvBusiness.Visible = true;
            }
        }

        protected void cusDuplicateEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (context.Users.Any(u => u.email == args.Value))
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            catch
            {
                args.IsValid = false;
            }
        }
    }
}