﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Data_A3;
using wcm = System.Web.Configuration.WebConfigurationManager;
using System.Text.RegularExpressions;

namespace A3_Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected CacheHelper ch;

        private A3_LINQDataContext context;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new A3_LINQDataContext();
            ch = new CacheHelper(this.Context);
            if (!IsPostBack)
            {
                if (ch.sessionUser != null)
                {
                    setLogin();
                }
            }
            else
            {
                if (ch.sessionUser != null)
                {
                    setLogin();
                }
                else
                {
                    setLogout();
                }
            }                       
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            bool found = false;
            User userLoggedIn;
            int numEmail = 0;
            if (IsValid)
            {
                foreach (User u in context.Users.ToList())
                {
                    if (!found)
                    {
                        if (u.email.ToLower().Equals(txtEmail.Text.ToLower()))
                        {
                            numEmail = 1;
                            if (u.password.Equals(txtPassword.Text))
                            {
                                found = true;
                                userLoggedIn = u;
                                ch.sessionUser = u;
                                setLogin();
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "passwordAlert", "alert('" + "Invalid Password" + "');", true);
                                setLogout();
                            }
                        }
                        else
                        {
                            setLogout();
                        }
                    }
                }
                if (numEmail == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "emailAlert", "alert('" + "Invalid Email" + "');", true);
                }
            }
        }

        private void setLogin()
        {            
                User user = ch.getSession();
                lblLoggedIn.Text = "Welcome, " + user.firstName + " " + user.lastName + " " + user.Business.businessName;

                lblLoggedIn.Visible = true;
                btnLogOut.Visible = true;

                lblEmail.Visible = false;
                lblPassword.Visible = false;
                txtEmail.Visible = false;
                txtPassword.Visible = false;
                btnLogin.Visible = false;            
        }

        private void setLogout()
        {
            lblLoggedIn.Visible = false;
            btnLogOut.Visible = false;

            lblEmail.Visible = true;
            lblPassword.Visible = true;
            txtEmail.Visible = true;
            txtPassword.Visible = true;
            btnLogin.Visible = true;
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            ch.removeSession();            
            setLogout();
        }


        protected void cusEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                args.IsValid = Regex.IsMatch(args.Value, wcm.AppSettings["emailRegex"]);
            }
            catch
            {
                args.IsValid = false;
            }
        }
    }
}
