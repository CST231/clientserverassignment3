﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="A3_Web.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div>
    <asp:Label id="lblEmail" runat="server" >Email Address:</asp:Label>
    <asp:TextBox ID="txtEmail" 
        runat="server">
    </asp:TextBox>
    <asp:RequiredFieldValidator
        ID="rfvEmail" runat="server"
        ErrorMessage="Email is Required"
        Display="Dynamic"
        ControlToValidate="txtEmail"
        CssClass="failureNotification">
    </asp:RequiredFieldValidator>

    <asp:CustomValidator ID="cusEmail" 
        runat="server" 
        CssClass="failureNotification"
        Display="Dynamic"
        ControlToValidate="txtEmail"
        ErrorMessage="Invalid email address entered" 
        onservervalidate="cusEmail_ServerValidate">
    </asp:CustomValidator>
</div>

<div>
    <asp:Label id="lblPassword" runat="server">Password:</asp:Label>
    <asp:TextBox ID="txtPassword" 
        runat="server" 
        TextMode="Password">
    </asp:TextBox>       
    <asp:RequiredFieldValidator
        ID="rfvPassword" 
        runat="server"
        ErrorMessage="Password is Required"
        Display="Dynamic"
        ControlToValidate="txtEmail"
        CssClass="failureNotification">
    </asp:RequiredFieldValidator> 
</div>

<div>
    <asp:Button ID="btnLogin" 
        runat="server" 
        Text="Log In" 
        onclick="btnLogin_Click" />
</div>

<div>
    <asp:Label ID="lblLoggedIn" 
        runat="server" 
        Visible="False" 
        Font-Bold="True" 
        Font-Size="Medium" 
        Font-Underline="True" 
        ForeColor="Blue">
    </asp:Label>
</div>

<div>
    <asp:Button ID="btnLogOut" 
        runat="server" 
        Text="Log Out" 
        Visible="false" 
        onclick="btnLogOut_Click" />
</div>

</asp:Content>
