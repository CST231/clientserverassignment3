﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Linq;
using Data_A3;

namespace A3_Web
{
    public partial class Reseservation : System.Web.UI.Page
    {
        protected CacheHelper ch;
        private A3_LINQDataContext context;

        private const string keySort = "Sort_gvReserve";
        private const string keyAscend = "IsAscending_gvReserve";

        private string ReserveSortField
        {
            get
            {
                return ViewState[keySort] == null ? "Title" : (string)ViewState[keySort];
            }
            set
            {
                if (value == null)
                {
                    ViewState.Remove(keySort);
                }
                else
                {
                    ViewState[keySort] = value;
                }
            }
        }

        private bool ReserveSortIsAscending
        {
            get
            {
                return ViewState[keyAscend] == null ? true : (bool)ViewState[keyAscend];
            }
            set
            {
                ViewState[keyAscend] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new A3_LINQDataContext();
            ch = new CacheHelper(this.Context);
            
            // Check if post back
            if (ch.sessionUser != null)
            {
                // Check if a user is logged in
                if (!IsPostBack)
                {
                    // Populate the drop down list
                    int i = DateTime.Now.Year, date = i + 2;
                    for (; i <= date; i++)
                    {
                        ddlYearSelect.Items.Add(i.ToString());
                    }
                    if (!ch.cookieUser.Equals(""))
                    {
                        ddlYearSelect.SelectedIndex = ddlYearSelect.Items.IndexOf(ddlYearSelect.Items.FindByValue(ch.cookieUser));
                        //ddlYearSelect.Items.FindByValue(ch.cookieUser).Selected = true;
                    }
                    else
                    {
                        ddlYearSelect.SelectedIndex = ddlYearSelect.Items.IndexOf(ddlYearSelect.Items.FindByValue(DateTime.Now.Year.ToString()));
                        //ddlYearSelect.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                        ch.cookieUser = ddlYearSelect.SelectedValue;
                    }
                    // If business ID is greater than 1 it is a business
                    if (ch.sessionUser.businessID > 1)
                    {
                        initGrid();
                    }
                    else
                    {
                        displayIncorrectUser();
                    }
                }
            }
            else
            {
                displayIncorrectUser();
            }
        }

        private void initGrid()
        {
            // Set to visible the controls
            lblCalendar.Visible = true;
            ddlYearSelect.Visible = true;
            gvReserve.Visible = true;
            lblError.Visible = false;
            int yearSelected = Convert.ToInt32(ddlYearSelect.SelectedItem.Value);
            var query =
                from reserve in context.Reservations
                join artwork in context.Artworks
                on reserve.artID equals artwork.artID
                //join user in context.Users
                //on reserve.businessID equals user.businessID
                where reserve.businessID == (ch.sessionUser.businessID)
                where reserve.year == yearSelected
                select new ArtReservation
                (
                    artwork.title,
                    artwork.rentalPrice,
                    reserve.quarter
                );

            Func<ArtReservation, object> sortSelector;

            switch (ReserveSortField)
            {
                case "Title":
                    sortSelector = AR => AR.title;
                    break;
                case "Rental_Price":
                    sortSelector = AR => AR.rentalPrice;
                    break;
                case "Quarter":
                    sortSelector = AR => AR.quarter;
                    break;
                default:
                    sortSelector = AR => AR.title;
                    break;
            };

            if (ReserveSortIsAscending)
            {
                gvReserve.DataSource = query.OrderBy(sortSelector).ToList();
            }
            else
            {
                gvReserve.DataSource = query.OrderByDescending(sortSelector).ToList();
            }

            gvReserve.DataBind();
        }

        protected void gvReserve_Sorting(object sender, GridViewSortEventArgs e)
        {
            ReserveSortIsAscending = ReserveSortField != e.SortExpression || !ReserveSortIsAscending;
            ReserveSortField = e.SortExpression;
            initGrid();
        }

        protected void gvReserve_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                switch (e.Row.Cells[2].Text)
                {
                    case "1":
                        e.Row.Cells[2].Text = "January to March";
                        break;
                    case "2":
                        e.Row.Cells[2].Text = "April to June";
                        break;
                    case "3":
                        e.Row.Cells[2].Text = "July to September";
                        break;
                    case "4":
                        e.Row.Cells[2].Text = "October to December";
                        break;
                }
            }
        }

        private void displayIncorrectUser()
        {
            lblError.Visible = true;
            lblCalendar.Visible = false;
            ddlYearSelect.Visible = false;
            gvReserve.Visible = false;
        }

        protected void ddlYearSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            ch.cookieUser = ddlYearSelect.SelectedItem.Value;
            initGrid();
        }
    }
}