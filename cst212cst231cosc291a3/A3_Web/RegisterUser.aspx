﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterUser.aspx.cs" Inherits="A3_Web.RegisterUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <label id="lblUserType">User Type:</label>
        <asp:DropDownList ID="ddlUserType" 
            runat="server" 
            AutoPostBack="True" 
            onselectedindexchanged="ddlUserType_SelectedIndexChanged">
            <asp:ListItem>Business</asp:ListItem>
            <asp:ListItem>Artist</asp:ListItem>
        </asp:DropDownList>
    </div>

    <div>
        <label id="lblFirstName">First Name:</label>
        <asp:TextBox ID="txtFirstName" 
            runat="server" 
            MaxLength="50">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvFirstName" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtFirstName"
            ErrorMessage="First Name is a required field">
        </asp:RequiredFieldValidator>
    </div>

    <div>
        <label id="lblLastName">Last Name:</label>
        <asp:TextBox ID="txtLastName" 
            runat="server" 
            MaxLength="50">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvLastName" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtLastName"
            ErrorMessage="Last Name is a required field">
        </asp:RequiredFieldValidator>
    </div>

    <div>
        <asp:Label ID="lblBusinessName" 
            runat="server"
            Text="Business Name:"></asp:Label>
        <asp:TextBox ID="txtBusinessName" 
            runat="server" 
            MaxLength="50">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvBusiness" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtBusinessName"
            ErrorMessage="Business Name is a required field">
        </asp:RequiredFieldValidator>
    </div>

    <div>
        <label id="lblEmailAddress">Email Address:</label>
        <asp:TextBox ID="txtEmailAddress" 
            runat="server" 
            MaxLength="100">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtEmailAddress"
            ErrorMessage="Email Address is a required field">
        </asp:RequiredFieldValidator>
        <asp:CustomValidator ID="cusEmail" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtEmailAddress"
            ErrorMessage="Invalid email address entered" 
            onservervalidate="cusEmail_ServerValidate">
        </asp:CustomValidator>
        <asp:CustomValidator ID="cusDuplicateEmail" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtEmailAddress"
            ErrorMessage="Email address has already been used" 
            onservervalidate="cusDuplicateEmail_ServerValidate">
        </asp:CustomValidator>
    </div>

    <div>
        <label id="lblPassword">Password:</label>
        <asp:TextBox ID="txtPassword" 
            runat="server" 
            MaxLength="16" 
            TextMode="Password">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvPassword" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtPassword"
            ErrorMessage="Password is a required field">
        </asp:RequiredFieldValidator>
        <asp:CompareValidator ID="cvPassword" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtPassword"
            ControlToCompare="txtConfirm"
            ErrorMessage="Passwords must match">
        </asp:CompareValidator>
        <asp:CustomValidator ID="cusPassword" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtPassword"
            ErrorMessage="Invalid password entered" 
            onservervalidate="cusPassword_ServerValidate">
        </asp:CustomValidator>
    </div>

    <div>
        <label id="lblConfirm">Confirm Password:</label>
        <asp:TextBox ID="txtConfirm" 
            runat="server" 
            MaxLength="16" 
            TextMode="Password">
        </asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvConfirm" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtPassword"
            ErrorMessage="Password is a required field">
        </asp:RequiredFieldValidator>
        <asp:CompareValidator ID="cvConfirm" 
            runat="server" 
            CssClass="failureNotification"
            Display="Dynamic"
            ControlToValidate="txtConfirm"
            ControlToCompare="txtPassword"
            ErrorMessage="Passwords must match">
        </asp:CompareValidator>
    </div>
    <div>
        <asp:Button ID="btnRegisterUser" 
            runat="server" 
            Text="Register" 
            onclick="btnRegisterUser_Click" />
    </div>
</asp:Content>
