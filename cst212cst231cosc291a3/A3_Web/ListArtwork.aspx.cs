﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Data_A3;
using System.Data.Linq;
using wcm = System.Web.Configuration.WebConfigurationManager;
using System.Text.RegularExpressions;

namespace A3_Web
{
    public partial class ListArtwork : System.Web.UI.Page
    {
        protected CacheHelper ch;
        private A3_LINQDataContext context;

        private const string keySort = "Sort_gvListArtwork";
        private const string keyAscend = "IsAscending_gvListArtwork";

        private int presentQuarter;

        private string ListArtSortField
        {
            get
            {
                return ViewState[keySort] == null ? "Title" : (string)ViewState[keySort];
            }
            set
            {
                if (value == null)
                {
                    ViewState.Remove(keySort);
                }
                else
                {
                    ViewState[keySort] = value;
                }
            }
        }

        private bool ListArtSortIsAscending
        {
            get
            {
                return ViewState[keyAscend] == null ? true : (bool)ViewState[keyAscend];
            }
            set
            {
                ViewState[keyAscend] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new A3_LINQDataContext();
            ch = new CacheHelper(this.Context);
            setPresentQuarter();

            if (!IsPostBack)
            {
                // Populate the drop down list
                int i = DateTime.Now.Year, date = i + 2;
                for (; i <= date; i++)
                {
                    ddlYearSelect.Items.Add(i.ToString());
                }
                if (!ch.cookieUser.Equals(""))
                {
                    ddlYearSelect.SelectedIndex = ddlYearSelect.Items.IndexOf(ddlYearSelect.Items.FindByValue(ch.cookieUser));
                    //ddlYearSelect.Items.FindByValue(ch.cookieUser).Selected = true;
                }
                else
                {
                    ddlYearSelect.SelectedIndex = ddlYearSelect.Items.IndexOf(ddlYearSelect.Items.FindByValue(DateTime.Now.Year.ToString()));
                    //ddlYearSelect.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                    ch.cookieUser = ddlYearSelect.SelectedValue;
                }
                initGrid();
            }
        }

        private void setPresentQuarter()
        {
            switch (DateTime.Now.Month)
            {
                case 1:
                case 2:
                case 3:
                    presentQuarter = 1;
                    break;
                case 4:
                case 5:
                case 6:
                    presentQuarter = 2;
                    break;
                case 7:
                case 8:
                case 9:
                    presentQuarter = 3;
                    break;
                case 10:
                case 11:
                case 12:
                    presentQuarter = 4;
                    break;
            }
        }

        private void initGrid()
        {
            // Set up gridview
            int yearSelected = Convert.ToInt32(ddlYearSelect.SelectedItem.Value);
            var query =
                from artwork in context.Artworks
                //join reserve in context.Reservations
                //on artwork.artID equals reserve.artID
                join artist in context.Users
                on artwork.userID equals artist.userID
                //where reserve.year == yearSelected
                select new ListReservation
                (
                    artwork.artID,
                    artwork.title,
                    artwork.description,
                    artwork.rentalPrice,
                    artist.firstName + " " + artist.lastName
                );

            // Sorting
            Func<ListReservation, object> sortSelector;

            switch (ListArtSortField)
            {
                case "Title":
                    sortSelector = LA => LA.title;
                    break;
                case "rentalPrice":
                    sortSelector = LA => LA.rentalPrice;
                    break;
                default:
                    sortSelector = LA => LA.title;
                    break;
            }

            if (ListArtSortIsAscending)
            {
                gvListArtwork.DataSource = query.Distinct().OrderBy(sortSelector).ToList();
            }
            else
            {
                gvListArtwork.DataSource = query.Distinct().OrderByDescending(sortSelector).ToList();
            }

            gvListArtwork.DataBind();
        }

        private void resetViews()
        {
            gvListArtwork_RowCancelingEdit(null, null);
            gvListArtwork.SelectedIndex = -1;
        }

        protected void gvListArtwork_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvListArtwork.EditIndex = -1;
            initGrid();
        }

        protected void gvListArtwork_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (ch.sessionUser != null)
            {
                if (ch.sessionUser.businessID == 1)
                {
                    gvListArtwork.EditIndex = e.NewEditIndex;
                }
            }
            else
            {
                gvListArtwork.EditIndex = -1;
            }
            initGrid();
        }

        protected void gvListArtwork_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int artID = (int)gvListArtwork.DataKeys[e.RowIndex].Value;
            Artwork art = context.Artworks.FirstOrDefault(A => A.artID == artID);
            if (art != null)
            {
                art.title = e.NewValues["Title"].Equals(art.title) ? art.title : (string)e.NewValues["Title"];
                art.rentalPrice = Convert.ToDecimal(e.NewValues["rentalPrice"]) == art.rentalPrice ? 
                    art.rentalPrice : Convert.ToDecimal(e.NewValues["rentalPrice"]);
                context.SubmitChanges();
            }
            gvListArtwork_RowCancelingEdit(null, null);
        }

        protected void gvListArtwork_Sorting(object sender, GridViewSortEventArgs e)
        {
            ListArtSortIsAscending = ListArtSortField != e.SortExpression || !ListArtSortIsAscending;
            ListArtSortField = e.SortExpression;
            initGrid();
        }

        protected void gvListArtwork_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            List<short> quarter = new List<short>();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ListReservation lr = (ListReservation)e.Row.DataItem;
                var query =
                    from reserve in context.Reservations
                    where reserve.artID == lr.artId
                    where reserve.year == Convert.ToInt32(ddlYearSelect.SelectedItem.Value)
                    select reserve.quarter;
                quarter = query.ToList();
                int selectedYear = Convert.ToInt32(ddlYearSelect.SelectedItem.Value);
                for (int i = 1; i < 5; i++)
                {
                    int selectQuarter = 0;
                    string businessName = "";
                    if (quarter.Contains((short)i))
                    {
                        selectQuarter = i;
                        var busName =
                            from reserve in context.Reservations
                            join business in context.Businesses
                            on reserve.businessID equals business.businessID
                            where reserve.artID == lr.artId
                            where reserve.quarter == (short)selectQuarter
                            where reserve.year == selectedYear
                            select business.businessName;
                        businessName = busName.First();
                    }
                    switch (selectQuarter)
                    {
                        case 1:
                            setQuarter(businessName, i, selectQuarter, selectedYear, e);
                            break;
                        case 2:
                            setQuarter(businessName, i, selectQuarter, selectedYear, e);
                            break;
                        case 3:
                            setQuarter(businessName, i, selectQuarter, selectedYear, e);
                            break;
                        case 4:
                            setQuarter(businessName, i, selectQuarter, selectedYear, e);
                            break;
                        default:
                            setQuarter("Not Reserved", i, i, selectedYear, e);
                            break;
                    }
                    //if (ch.sessionUser != null && ch.sessionUser.businessID == 1)
                    //{
                    //    var artistQuery =
                    //        from artwork in context.Artworks
                    //        where artwork.artID == lr.artId
                    //        select artwork.userID;
                    //    if (artistQuery.First() == ch.sessionUser.userID)
                    //    {
                    //        if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                    //        {
                    //            TextBox txtTitle = (TextBox)e.Row.FindControl("txtTitle");
                    //            TextBox txtRentalPrice = (TextBox)e.Row.FindControl("txtRentalPrice");
                    //            if (txtTitle != null && txtRentalPrice != null)
                    //            {
                    //                txtTitle.Text = lr.title;
                    //                txtRentalPrice.Text = lr.rentalPrice.ToString();
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
        }

        private void setQuarter(string name, int i, int quarter, int selectedYear, GridViewRowEventArgs e)
        {
            ListReservation lr = (ListReservation)e.Row.DataItem;
            // If quarter has passed or if we're presently in it
            if (quarter <= presentQuarter && DateTime.Now.Year == selectedYear)
            {
                e.Row.Cells[i + 3].Text = name;
                e.Row.Cells[i + 3].Enabled = false;
            }
            else
            {
                // Check if user is logged in
                if (ch.sessionUser != null)
                {
                    // Check if artist or business
                    // If business
                    if (ch.sessionUser.businessID > 1)
                    {
                        // If business logged in is the business that rented the piece in that quarter
                        if (ch.sessionUser.Business.businessName.Equals(name))
                        {
                            LinkButton lnkQuarter = (LinkButton)e.Row.Cells[i + 3].FindControl("lnkQuarter" + quarter);
                            if (lnkQuarter != null)
                            {
                                lnkQuarter.Enabled = true;
                            }
                            lnkQuarter.Text = ch.sessionUser.Business.businessName;
                            lnkQuarter.Attributes.Add("artId", lr.artId.ToString());
                            lnkQuarter.Attributes.Add("quarter", quarter.ToString());
                            lnkQuarter.Attributes.Add("year", selectedYear.ToString());
                        }
                        // Otherwise if it is not reserved
                        else if (name.Equals("Not Reserved"))
                        {
                            LinkButton lnkQuarter = (LinkButton)e.Row.Cells[i + 3].FindControl("lnkQuarter" + quarter);
                            if (lnkQuarter != null)
                            {
                                lnkQuarter.Enabled = true;
                            }
                            lnkQuarter.Text = "Not Reserved";
                            lnkQuarter.Attributes.Add("artId", lr.artId.ToString());
                            lnkQuarter.Attributes.Add("quarter", quarter.ToString());
                            lnkQuarter.Attributes.Add("year", selectedYear.ToString());
                        }
                        else
                        {
                            e.Row.Cells[i + 3].Text = name;
                            e.Row.Cells[i + 3].Enabled = false;
                        }
                    }
                    // If artist
                    else
                    {
                        e.Row.Cells[i + 3].Text = name;
                        e.Row.Cells[i + 3].Enabled = false;
                    }
                }
                else
                {
                    e.Row.Cells[i + 3].Text = name;
                    e.Row.Cells[i + 3].Enabled = false;
                }
            }
        }

        protected void ddlYearSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            ch.cookieUser = ddlYearSelect.SelectedItem.Value;
            gvListArtwork_RowCancelingEdit(null, null);
            initGrid();
        }

        protected void gvListArtwork_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Quarter1":
                case "Quarter2":
                case "Quarter3":
                case "Quarter4":
                    LinkButton resButton = (LinkButton)e.CommandSource;
                    int artId = Convert.ToInt32(resButton.Attributes["artId"]);
                    short selectedQuarter = Convert.ToInt16(resButton.Attributes["quarter"]);
                    int selectedYear = Convert.ToInt32(resButton.Attributes["year"]);
                    // If not reserved, set reservation
                    if (resButton.Text.Equals("Not Reserved"))
                    {
                        Reservation reserve = new Reservation
                        {
                            artID = artId,
                            businessID = ch.sessionUser.businessID,
                            quarter = selectedQuarter,
                            year = selectedYear
                        };
                        context.Reservations.InsertOnSubmit(reserve);
                        context.SubmitChanges();
                    }
                    // Cancel reservation
                    else
                    {
                        var delete =
                            from res in context.Reservations
                            where res.artID == artId
                            where res.quarter == selectedQuarter
                            where res.year == selectedYear
                            where res.businessID == ch.sessionUser.businessID
                            select res.reservationID;
                        int resDelete = delete.First();
                        removeReservation(resDelete);
                    }
                    break;
            }
            initGrid();
        }

        private void removeReservation(int resDelete)
        {
            Reservation res = context.Reservations.Where(R => R.reservationID == resDelete).First();
            context.Reservations.DeleteOnSubmit(res);
            context.SubmitChanges();
        }
    }
}