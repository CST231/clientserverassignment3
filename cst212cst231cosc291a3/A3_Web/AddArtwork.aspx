﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddArtwork.aspx.cs" Inherits="A3_Web.AddArtwork" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlAddArtwork" 
        runat="server">
        <div>
            <label>Title: </label>
            <asp:TextBox ID="txtTitle" 
                runat="server" 
                MaxLength="50">
            </asp:TextBox>
        </div>

        <div>
            <label>Description: </label>
            <asp:TextBox ID="txtDescription" 
                runat="server" 
                TextMode="MultiLine" 
                MaxLength="100">
            </asp:TextBox>
        </div>

        <div>
            <label>Price: </label>
            <asp:TextBox ID="txtPrice" 
                runat="server">
            </asp:TextBox>
        </div>

        <div>
            <asp:Button ID="btnSave" 
                runat="server" 
                Text="Save" 
                onclick="btnSave_Click" />
        </div>
    </asp:Panel>
    <asp:Label ID="lblNotArtist" 
        runat="server" 
        Visible="false"
        Text="You must be logged in as an artist to add artwork!">
    </asp:Label>
</asp:Content>
