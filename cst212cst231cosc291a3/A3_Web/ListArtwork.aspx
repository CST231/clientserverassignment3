﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListArtwork.aspx.cs" Inherits="A3_Web.ListArtwork" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:Label ID="lblCalendar" 
            runat="server" 
            Text="Select Date">
        </asp:Label>
        <asp:DropDownList ID="ddlYearSelect" 
            runat="server"
            AutoPostBack="true" 
            onselectedindexchanged="ddlYearSelect_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <div>
        <asp:GridView ID="gvListArtwork" 
            runat="server"
            AllowSorting="true"
            AutoGenerateColumns="false" 
            DataKeyNames="artID"
            onrowcancelingedit="gvListArtwork_RowCancelingEdit" 
            onrowediting="gvListArtwork_RowEditing" 
            onrowupdating="gvListArtwork_RowUpdating" 
            onsorting="gvListArtwork_Sorting"
            onrowdatabound="gvListArtwork_RowDataBound" 
            onrowcommand="gvListArtwork_RowCommand">
            <Columns>
                <asp:TemplateField HeaderText="Title" SortExpression="Title">
                    <EditItemTemplate>
                        <asp:LinkButton ID="lnkTitleUpdate"
                            runat="server"
                            Text="Update"
                            CommandName="Update">
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkTitleCancel"
                            runat="server"
                            Text="Cancel"
                            CommandName="Cancel">
                        </asp:LinkButton>
                        <asp:TextBox ID="txtTitle"
                            runat="server"
                            Text='<%# Bind("Title") %>'
                            Enabled="true">
                        </asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkTitle"
                            runat="server"
                            Text='<%# Eval("Title") %>'
                            CommandName="Edit"
                            Enabled="true">
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField ReadOnly="true" 
                    DataField="Description"
                    HeaderText="Description" />

                <asp:TemplateField HeaderText="Rental Price" SortExpression="rentalPrice">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtRentalPrice"
                            runat="server"
                            Text='<%# Bind("rentalPrice") %>'
                            Enabled="true">
                        </asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <%# Eval("rentalPrice") %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField ReadOnly="true" 
                    DataField="Artist"
                    HeaderText="Artist" />

                <asp:TemplateField HeaderText="Quarter 1">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkQuarter1"
                            runat="server"
                            Enabled="true"
                            CommandName="Quarter1"
                            CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Quarter 2">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkQuarter2"
                            runat="server"
                            Enabled="true"
                            CommandName="Quarter2"
                            CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Quarter 3">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkQuarter3"
                            runat="server"
                            Enabled="true"
                            CommandName="Quarter3"
                            CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Quarter 4">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkQuarter4"
                            runat="server"
                            Enabled="true"
                            CommandName="Quarter4"
                            CommandArgument='<%#((GridViewRow)Container).RowIndex %>'>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
