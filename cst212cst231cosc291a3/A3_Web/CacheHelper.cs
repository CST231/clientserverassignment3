﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data_A3;

namespace A3_Web
{
    public class CacheHelper
    {
        //contains environment information from the web page object
        private HttpContext context;

        //define a constant for each different session or cookie variable
        private const string keyUserObject = "user_object";
        private const string selectedYear = "selectedYear";

        public User sessionUser
        {
            get { return getSession(); }
            set
            {
                if (value == null)
                {
                    removeSession();
                }
                else
                {
                    setSession(value);
                }
            }
        }

        public string cookieUser
        {
            get { return getCookie(selectedYear); }
            set
            {
                if (value == null)
                {
                    removeCookie(selectedYear);
                }
                else
                {
                    setCookie(selectedYear, value);
                }
            }
        }

        public CacheHelper(HttpContext pageContext)
        {
            context = pageContext;
        }

        //methods to set, clear and get values from session and cookies
        public User getSession()
        {
            //return value from session
            //this would be available as soon as it is set in code
            return (User)context.Session[keyUserObject];
        }

        public void setSession(User user)
        {
            //set the session variable
            context.Session[keyUserObject] = user;
        }

        public void removeSession()
        {
            //multiple ways to remove session
            context.Session.Remove(keyUserObject);

            //context.Session.Clear(); //removes ALL session variables
            //context.Session.Abandon(); //cancels the session - great for
        }

        public string getCookie(string strKey)
        {
            //return a value from a cookie if it exists
            //IMPORTANT: use the ***Request*** object to get the cookie
            //the broswer has to send the cookie in the Request to the server
            return context.Request.Cookies[strKey] == null ? "" /*null*/ : context.Request.Cookies[strKey].Value;

        }

        public void setCookie(string strKey, string strVal)
        {
            //set cookie
            //IMPORTANT: use the **Response** Object to set the cookie
            //the server has to tell the browser to save the cookie
            context.Response.Cookies[strKey].Value = strVal;
            //for this example we expire the cookie after 1 day
            context.Response.Cookies[strKey].Expires = DateTime.Today.AddDays(3);
        }

        public void removeCookie(string strKey)
        {
            context.Response.Cookies[strKey].Expires = DateTime.Today.AddDays(-5);
        }

    }
}