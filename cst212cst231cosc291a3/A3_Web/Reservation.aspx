﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reservation.aspx.cs" Inherits="A3_Web.Reseservation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:Label ID="lblCalendar" 
            runat="server" 
            Text="Select Date">
        </asp:Label>
        <asp:DropDownList ID="ddlYearSelect" 
            runat="server"
            AutoPostBack="True" 
            onselectedindexchanged="ddlYearSelect_SelectedIndexChanged">
        </asp:DropDownList>
    </div>
    <div>
        <asp:GridView ID="gvReserve" 
            runat="server"
            AllowSorting="True"
            onsorting="gvReserve_Sorting"
            AutoGenerateColumns="false"
            onrowdatabound="gvReserve_RowDataBound">
            <Columns>
                <asp:BoundField DataField="Title" 
                    SortExpression="Title" 
                    HeaderText="Title" />
                <asp:BoundField DataField="RentalPrice" 
                    SortExpression="Rental_Price" 
                    HeaderText="Rental Price"
                    DataFormatString="{0:C}" />
                <asp:BoundField DataField="Quarter" 
                    SortExpression="Quarter" 
                    HeaderText="Quarter" />
            </Columns>
        </asp:GridView>
    </div>
    <div>
        <asp:Label runat="server" 
            Text="You must be logged in as a business to view reserved artwork!" 
            ID="lblError">
        </asp:Label>
    </div>
</asp:Content>
